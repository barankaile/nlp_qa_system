import csv
from qa_engine.base import QABase
from qa_engine.score_answers import main as score_answers
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer
from dependency_demo_stub import  find_main
from collections import defaultdict


import nltk, operator

STOPWORDS = set(nltk.corpus.stopwords.words("english"))
WN_MAP = {
    "J":wn.ADJ, 
    "V": wn.VERB, 
    "N": wn.NOUN, 
    "R":wn.ADV
}

MAPPER = {
        "who": "(NP)", 
        "did":"(NP (*))", 
        "had":"(ADJP)", 
        "when": "(PP (*))", 
        "where": "(PP (*))", 
        "what":"(VP)",
        "what did": "(VP (*))",
        "how":"(NP (NP))", 
        "which":"(NP (VP))", 
        "why": "(VP)"
}

LEMMATIZER = WordNetLemmatizer()

DATA_DIR = "./wordnet"

def load_wordnet_ids(filename):
    file = open(filename, 'r')
    if "noun" in filename: type = "noun"
    else: type = "verb"
    csvreader = csv.DictReader(file, delimiter=",", quotechar='"')
    word_ids = defaultdict()
    for line in csvreader:
        word_ids[line['synset_id']] = {'synset_offset': line['synset_offset'], 'story_'+type: line['story_'+type], 'stories': line['stories']}
    return word_ids

# The standard NLTK pipeline for POS tagging a document
def get_sentences(text):
    sentences = nltk.sent_tokenize(text)
    sentences = [nltk.word_tokenize(sent) for sent in sentences]
    sentences = [nltk.pos_tag(sent) for sent in sentences]

    return sentences


def get_bow(tagged_tokens, stopwords):
    return set([t[0].lower() for t in tagged_tokens if t[0].lower() not in stopwords])

#returns a lemmatized bag of words
def get_lem_bow(sentence, stopwords):
    s_lem = []
    for word in sentence:
        if word[1][0] in WN_MAP:
            s_lem.append(LEMMATIZER.lemmatize(word[0], WN_MAP[word[1][0]]))
        else:
            s_lem.append(LEMMATIZER.lemmatize(word[0]))
    return set([t.lower() for t in s_lem if t.lower() not in stopwords])


#returns a list of the bigrams contained within tagged_tokens excluding stopwords
def get_bigrams(tagged_tokens):
    bigrams = [token for token in tagged_tokens]
    
    return nltk.bigrams(bigrams)


def find_phrase(tagged_tokens, qbow):
    for i in range(len(tagged_tokens) - 1, 0, -1):
        word = (tagged_tokens[i])[0]
        if word in qbow:
            return tagged_tokens[i + 1:]
def hardq(main_word, q_text, s_tok, s_tree):
    noun_ids = load_wordnet_ids("{}/{}".format(DATA_DIR, "Wordnet_nouns.csv"))
    verb_ids = load_wordnet_ids("{}/{}".format(DATA_DIR, "Wordnet_verbs.csv"))
    wnorm = main_word[0][0]
    wlower = main_word[0][0].lower()
    main_synsets = wn.synsets(wlower)
    question = q_text
    alt_list = []
    for main_synset in main_synsets:
        main_hypo = main_synset.hyponyms()
        for hypo in main_hypo:
            htemp = hypo.name()[0:hypo.name().index(".")]
            if "_" in htemp:
                htemp = htemp.replace("_", " ")
            alt_list.append(htemp)
    potential_questions = []
    if alt_list:
        for alt in alt_list:
            altq = question.replace(wnorm, alt)
            potential_questions.append((altq,alt))
    scored_questions = []
    if potential_questions:
        for question in potential_questions:
            qbow = get_bow(get_sentences(question[0])[0], STOPWORDS)
            qlem = get_lem_bow(get_sentences(question[0])[0], STOPWORDS)
            
            sent, tree, score = baseline((question[1],main_word[0][1]), qlem, qbow, s_tok, STOPWORDS, s_tree, True)
            scored_questions.append((sent,tree,score))
        scored_questions = sorted(scored_questions, key=operator.itemgetter(2), reverse=True)
        best_answer = scored_questions[0][0]
        best_answer_tree = scored_questions[0][1]
        return best_answer, best_answer_tree
    else:
        qbow = get_bow(get_sentences(q_text)[0], STOPWORDS)
        qlem = get_lem_bow(get_sentences(q_text)[0], STOPWORDS)
        return baseline(main_word, qlem, qbow, s_tok, STOPWORDS, s_tree)
    
      
def getkey(item):
    return item[2]

# qtokens: is a list of pos tagged question tokens with SW removed
#qlem wn lemmatized question
# sentences: is a list of pos tagged story sentences
# stopwords is a set of stopwords
def baseline(mainv, qlem, qbow, sentences, stopwords, trees, hardq=False):
     
    answers = []
    vlem = get_lem_bow(mainv, stopwords)
    tree_dict = {}
    for idx, sent in enumerate(sentences):
        # A list of all the word tokens in the sentence
        sbow = get_bow(sent, stopwords)
        slem = get_lem_bow(sent, stopwords) #lemmatize the sentence
        # Count the # of overlapping words between the Q and the A
        # & is the set intersection operator
        #sum this count w/ the # of overlapping lemmatized words between the Q and the A
        #add to count if d word present
        intersect = len(qlem & slem) + len(qbow & sbow)
        if len(slem & vlem) == 1 or len(vlem & sbow) == 1:
            intersect += 10
            
        ############# This code is only to fix a bug that I believe exists in the eval file #################
        ############# where one of the stories had a list of sentences longer than the list of sentence parse trees ##############
        ############ if the eval file is modified after the due date and my program crashes###########
        ########### please remove these next 2 lines and rerun it, thank you ######################
        if idx > len(trees)-1:
            idx = len(trees)-1
        ########################################################################
        #######################################################################    
        
        answers.append((intersect, sent, trees[idx]))

    # Sort the results by the first element of the tuple (i.e., the count)
    # Sort answers from smallest to largest by default, so reverse it
    answers = sorted(answers, key=operator.itemgetter(0), reverse=True)

    # Return the best answer
    best_answer_intersect = answers[0][0]
    best_answer = answers[0][1]
    best_answer_tree = answers[0][2]
    if not hardq:
        return best_answer, best_answer_tree 
    else: 
        return best_answer, best_answer_tree, best_answer_intersect
        
def get_answer(question, story):
    """
    :param question: dict
    :param story: dict
    :return: str

    question is a dictionary with keys:
        dep -- A list of dependency graphs for the question sentence.
        par -- A list of constituency parses for the question sentence.
        text -- The raw text of story.
        sid --  The story id.
        difficulty -- easy, medium, or hard
        type -- whether you need to use the 'sch' or 'story' versions
                of the .
        qid  --  The id of the question.


    story is a dictionary with keys:
        story_dep -- list of dependency graphs for each sentence of
                    the story version.
        sch_dep -- list of dependency graphs for each sentence of
                    the sch version.
        sch_par -- list of constituency parses for each sentence of
                    the sch version.
        story_par -- list of constituency parses for each sentence of
                    the story version.
        sch --  the raw text for the sch version.
        text -- the raw text for the story version.
        sid --  the story id
    """
    ###     Your Code Goes Here         ###
    
    #saving raw text and parse for question
    qtext = question["text"]
    q_tokenized = get_sentences(qtext)
    q_word = q_tokenized[0][0][0].lower()
    if q_tokenized[0][0][0].lower() == "what" and q_tokenized[0][1][0] == "did":
        q_word = "what did"
    qpar = question["par"]
    qdep = question["dep"]
    main_dep = find_main(qdep)['word']
    tdep = []
    qdiff = question['difficulty']
    for word in q_tokenized[0]:
        if word[0] == main_dep:
            tdep.append(word)

    stext = ""
    stree = None
    if question['type'] == 'Sch':
        stext = story["sch"]
        stree = story["sch_par"]

    else:
        stext = story['text']  
        stree = story["story_par"]
 
        
    #regular bow
    qbow = get_bow(get_sentences(qtext)[0], STOPWORDS)
    #lem bow
    qlem = get_lem_bow(get_sentences(qtext)[0], STOPWORDS)
    #tokenized story
    sentences = get_sentences(stext)
    
    if qdiff == "Hard":
        answer, answer_tree = hardq(tdep, qtext, sentences, stree)
    else:
        #best sentence as text, best sentence tree
        answer, answer_tree = baseline(tdep, qlem, qbow, sentences, STOPWORDS, stree)
    #tree representation of answer pattern
    answer_pattern = nltk.Tree.fromstring(MAPPER[q_word])
    
    sub = find_subtree(answer_pattern, answer_tree)
        
    return sub if sub else " ".join(t[0] for t in answer)

def correct_sub(sub_ptree, subtree):
    return subtree if subtree.label() == sub_ptree.label() or sub_ptree.label() == "*" else None

#go through the tree and check which subtrees match the answer pattern
def find_subtree(ptree, trees):
    strees = []
    #check for subtrees w/ root matching either answer pattern or universal character            
    for tree in trees.subtrees(filter=lambda n: n.label() == ptree.label() or ptree.label == "*"):
        #once a valid root is identified, check if its children match the answer pattern
        for sub in ptree.subtrees():
            node = correct_sub(sub, tree)
            if node is not None:
                strees.append(node)
    return " ".join(strees[0].leaves()) if strees else None
        

#############################################################
###     Dont change the code in this section
#############################################################
class QAEngine(QABase):
    @staticmethod
    def answer_question(question, story):
        answer = get_answer(question, story)
        return answer


def run_qa(evaluate=False):
    QA = QAEngine(evaluate=evaluate)
    QA.run()
    QA.save_answers()

#############################################################


def main():
    # set evaluate to True/False depending on whether or
    # not you want to run your system on the evaluation
    # data. Evaluation data predictions will be saved
    # to hw6-eval-responses.tsv in the working directory.
    run_qa(evaluate=False)
    # You can uncomment this next line to evaluate your
    # answers, or you can run score_answers.py
    score_answers()

if __name__ == "__main__":
    main()
