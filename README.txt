README
Program functions as a question answering system that is able to respond to select questions about the contents of specific corpora.
Team members:
	Kaile Baran (kbaran)
	
Program Notes:
	Run program by typing python3 qa.py into the terminal.
	Currently, run_qa is set to false, and score_answers() is uncommented.